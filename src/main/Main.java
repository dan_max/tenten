package main;
import java.awt.EventQueue;

import model.DragMouse;
import model.Model;
import view.*;
public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            Model game = new Model();
            Panel app = new Panel(game);
            DragMouse me = new DragMouse(game);
            app.addMouseListener(me);
            app.setVisible(true);

        });

    }
}