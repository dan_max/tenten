package view;

import javax.swing.*;
import java.awt.*;

public class FieldPanel extends JPanel {
    private int[][] table;
    public FieldPanel(int[][] table_){
        setMinimumSize(new Dimension(400, 400));
        setSize(400, 400);
        table = table_;
    }

    public void paint(Graphics g){
        int x = 5, y = 5, step = 38;
        for(int i = 0; i < 10;  ++i){
            for(int j = 0; j < 10; ++j){
                if(table[i][j] == 1){
                    g.setColor(new Color(182,118,9));
                    g.fillRoundRect(x,y, 29 ,29 ,2, 2);
                }
                g.setColor(new Color(0,0,0));
                g.drawRoundRect(x,y, 29 ,29 ,2, 2);
                x += step;
            }
            x = 5;
            y +=step;
        }
    }
}
