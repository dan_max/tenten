package view;
import javax.swing.*;
import model.*;

import java.awt.*;

public class Panel extends JFrame{
    private Model game;
    public Panel(Model game_){
        super("1010!");
        game = game_;
        setMinimumSize(new Dimension(600,600));
        setBounds(100,100,600,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setResizable(false);
        JSplitPane main = new JSplitPane(JSplitPane.VERTICAL_SPLIT, game.getRecordPanel() ,game.getFieldPanel());
        main.setDividerSize(0);
        main.setDividerLocation(210);


        add(game.getFigurePanel(), BorderLayout.EAST);
        add(main, BorderLayout.CENTER);
        pack();
    }

}
