package view;

import javax.swing.*;
import model.Figure;


import java.awt.*;

public class FigureView extends JPanel{
    private Figure figure;
    public FigureView(Figure figure_){
        setPreferredSize(new Dimension(200, 200));
        setMinimumSize(new Dimension(200,200));
        setSize(200,200);
        figure = figure_;
    }
    public void paint(Graphics g){
        int[][] table = figure.getTable();
        int x = 5, y = 5, step = 38;
        for(int i = 0; i < figure.getSize_x(); ++i){
            for (int j = 0; j < figure.getSize_y(); ++j){
                if(table[i][j] == 1){
                    g.setColor(new Color(182, 118, 9));
                    g.fillRoundRect(x,y,29,29,2,2);
                    g.setColor(new Color(0,0,0));
                    g.drawRoundRect(x,y,29,29,2,2);
                }
                x += step;
            }
            x = 5;
            y += step;
        }
    }
}
