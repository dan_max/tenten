package view;

import javax.swing.*;
import java.awt.*;

public class RecordPanel extends JPanel {
    private int score, highScore;
    private JComponent curr_rec = new JLabel(Integer.toString(score));
    private JComponent rec = new JLabel(Integer.toString(highScore));
    public RecordPanel(int score_, int highScore_){
        score = score_;
        highScore = highScore_;
        setLayout(new GridLayout(1,2));
        setSize(400,100);
        setMinimumSize(new Dimension(400,100));


        ((JLabel) rec).setHorizontalAlignment(JLabel.CENTER);
        ((JLabel) curr_rec).setHorizontalAlignment(JLabel.CENTER);
        add(curr_rec);
        add(rec);
    }
    public void score_add(int v){
        score += v;
        curr_rec.repaint();
        rec.repaint();
        repaint();
    }

    public int getScore() {
        return score;
    }

    public int getHighScore() {
        return highScore;
    }
}
