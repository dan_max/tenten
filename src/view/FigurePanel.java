package view;

import model.Figure;

import javax.swing.*;
import java.awt.*;

public class FigurePanel extends JPanel {
    private FigureView f1_view, f2_view, f3_view;
    public FigurePanel(Figure f1_, Figure f2_, Figure f3_){
        setMinimumSize(new Dimension(200,600));
        setSize(200,600);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        f1_view = new FigureView(f1_);
        f2_view = new FigureView(f2_);
        f3_view = new FigureView(f3_);
        add(f1_view);
        add(f2_view);
        add(f3_view);
    }
    /*public void repaint(){
        f1_view.repaint();
        f2_view.repaint();
        f3_view.repaint();
    }*/

    public FigureView getF1_view() {
        return f1_view;
    }

    public FigureView getF2_view() {
        return f2_view;
    }

    public FigureView getF3_view() {
        return f3_view;
    }
}
