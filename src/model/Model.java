package model;

import view.FieldPanel;
import view.FigurePanel;
import view.RecordPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Model {
    private int[][] field = new int[10][10];
    private Figure f1, f2, f3;
    private FigurePanel figurepanel;
    private FieldPanel fieldPanel;
    private RecordPanel recordPanel;


    public Model() {
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                field[i][j] = 0;
            }
        }
        addFigures();
        figurepanel = new FigurePanel(f1, f2, f3);
        fieldPanel = new FieldPanel(field);
        recordPanel = new RecordPanel(0,0);

    }
    public void deleteFigure(int number){
        if(number == 1){
            f1.delete();
        }
        if(number == 2){
            f2.delete();
        }
        if(number == 3){
            f3.delete();
        }
    }

    public int setFigure(int[] click){
        Figure set_figure = f1;
        int y = 0;
        int ch = 0;
        if(click[1] > 35 && click[1] < 220){
            set_figure = f1;
            y = click[1] - 37;
        }

        if(click[1] > 236 && click[1] < 417){
            set_figure = f2;
            y = click[1] - 236;
        }

        if(click[1] > 435 && click[1] < 617){
            set_figure = f3;
            y = click[1] - 435;
        }
        int x = click[0] - 397;
        x = click[2] - x;
        y = click[3] - y;
        if(x <= 0 || y <= 0){
            return 0;
        }
        int i = -1 , j = -1;
        if(x > 0 && x <= 47){
            i = 0;
        }
        if(x >= 48 && x <= 85) {
            i = 1;
        }
        if(x >= 86 && x <= 124){
            i = 2;
        }
        if(x >= 125 && x <= 162){
            i = 3;
        }
        if(x >= 163 && x <= 199){
            i = 4;
        }
        if(x >= 200 && x <= 237){
            i = 5;
        }
        if(x >= 238 && x <= 275){
            i = 6;
        }
        if(x >= 276 && x <= 313){
            i = 7;
        }
        if(x >= 314 && x <= 351){
            i = 8;
        }
        if(x >= 352 && x <= 390){
            i = 9;
        }

        if(y >= 242 && y <= 279){
            j = 0;
        }
        if(y >= 280 && y <= 317){
            j = 1;
        }
        if(y >= 318 && y <= 355){
            j = 2;
        }
        if(y >= 356 && y <= 393){
            j = 3;
        }
        if(y >= 394 && y <= 431){
            j = 4;
        }
        if(y >= 432 && y <= 469){
            j = 5;
        }
        if(y >= 470 && y <= 508){
            j = 6;
        }
        if(y >= 509 && y <= 545){
            j = 7;
        }
        if(y >= 546 && y <= 584){
            j = 8;
        }
        if(y >= 585 && y <= 621){
            j = 9;
        }
        System.out.println(i+""+j);
        boolean can_add = true;
        int [][] table = set_figure.getTable();
        for(int c_i = 0; c_i < set_figure.getSize_x(); c_i++) {
            for(int c_j = 0; c_j < set_figure.getSize_y(); c_j++) {
                if(c_i + i > 9 && c_i + i < 0 || c_j + j > 9 && c_j + j < 0){
                    can_add = false;
                    return 0;
                }
                if(field[i + c_i][j + c_j] == 1 && table[c_i][c_j] == 1){
                    can_add = false;
                    return 0;
                }
            }
        }
        System.out.println(i + " " + j + "||" + set_figure.getSize_x() + " " + set_figure.getSize_y());
        if(can_add == true){
            for(int c_i = 0; c_i < set_figure.getSize_x(); c_i++) {
                for(int c_j = 0; c_j < set_figure.getSize_y(); c_j++) {
                    field[i + c_i][j + c_j] = table[c_i][c_j];
                }
            }

        }
        recordPanel.score_add(set_figure.getSize());
        set_figure.delete();
        for(i = 0; i < 10; i++){
            for(j = 0; j < 10; j++){
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        System.out.println(recordPanel.getScore());
        //fieldPanel.repaint();
        //figurepanel.repaint();

        check_fill();

        return 1;
    }

    public void check_fill(){
        int i = 0, j = 0;
        boolean c0 = true,c1 = true,c2 = true,c3 = true,c4 = true,c5 = true,c6 = true,c7 = true,c8 = true,c9 = true,r0 = true,r1 = true,r2 = true,r3 = true,r4 = true,r5 = true,r6 = true,r7 = true,r8 = true,r9 = true;

        for(i = 0 ; i < 10; ++i){
            if(field[0][i] == 0){
                r0 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[1][i] == 0){
                r1 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[2][i] == 0){
                r2 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[3][i] == 0){
                r3 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[4][i] == 0){
                r4 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[5][i] == 0){
                r5 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[6][i] == 0){
                r6 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[7][i] == 0){
                r7 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[8][i] == 0){
                r8 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[9][i] == 0){
                r9 = false;
            }
        }


        for(i = 0 ; i < 10; ++i){
            if(field[i][0] == 0){
                c0 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][1] == 0){
                c1 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][2] == 0){
                c2 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][3] == 0){
                c3 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][4] == 0){
                c4 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][5] == 0){
                c5 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][6] == 0){
                c6 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][7] == 0){
                c7 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][8] == 0){
                c8 = false;
            }
        }
        for(i = 0 ; i < 10; ++i){
            if(field[i][9] == 0){
                c9 = false;
            }
        }

        if(r0){
            for(i = 0 ; i < 10; ++i){
                field[0][i] = 0;
                recordPanel.score_add(10);
                r0 = false;
            }
        }
        if(r1){
            for(i = 0 ; i < 10; ++i){
                field[1][i] = 0;
                recordPanel.score_add(10);
                r1 = false;
            }
        }
        if(r2){
            for(i = 0 ; i < 10; ++i){
                field[2][i] = 0;
                recordPanel.score_add(10);
                r2 = false;
            }
        }
        if(r3){
            for(i = 0 ; i < 10; ++i){
                field[3][i] = 0;
                recordPanel.score_add(10);
                r2 = false;
            }
        }if(r4){
            for(i = 0 ; i < 10; ++i){
                field[4][i] = 0;
                recordPanel.score_add(10);
                r4 = false;
            }
        }if(r5){
            for(i = 0 ; i < 10; ++i){
                field[5][i] = 0;
                recordPanel.score_add(10);
                r5 = false;
            }
        }
        if(r6){
            for(i = 0 ; i < 10; ++i){
                field[6][i] = 0;
                recordPanel.score_add(10);
                r6 = false;
            }
        }
        if(r7){
            for(i = 0 ; i < 10; ++i){
                field[7][i] = 0;
                recordPanel.score_add(10);
                r7 = false;
            }
        }
        if(r8){
            for(i = 0 ; i < 10; ++i){
                field[8][i] = 0;
                recordPanel.score_add(10);
                r8 = false;
            }
        }
        if(r9){
            for(i = 0 ; i < 10; ++i){
                field[9][i] = 0;
                recordPanel.score_add(10);
                r9 = false;
            }
        }
        if(c0){
            for(i = 0 ; i < 10; ++i){
                field[i][0] = 0;
                recordPanel.score_add(10);
                c0 = false;
            }
        }
        if(c1){
            for(i = 0 ; i < 10; ++i){
                field[i][1] = 0;
                recordPanel.score_add(10);
                c1 = false;
            }

        }
        if(c2){
            for(i = 0 ; i < 10; ++i){
                field[i][2] = 0;
                recordPanel.score_add(10);
                c2 = false;
            }

        }
        if(c3){
            for(i = 0 ; i < 10; ++i){
                field[i][3] = 0;
                recordPanel.score_add(10);
                c3 = false;
            }

        }
        if(c4){
            for(i = 0 ; i < 10; ++i){
                field[i][4] = 0;
                recordPanel.score_add(10);
                c4 = false;
            }

        }
        if(c5){
            for(i = 0 ; i < 10; ++i){
                field[i][5] = 0;
                recordPanel.score_add(10);
                c5 = false;
            }

        }
        if(c6){
            for(i = 0 ; i < 10; ++i){
                field[i][6] = 0;
                recordPanel.score_add(10);
                c6 = false;
            }

        }
        if(c7){
            for(i = 0 ; i < 10; ++i){
                field[i][7] = 0;
                recordPanel.score_add(10);
                c7 = false;
            }

        }
        if(c8){
            for(i = 0 ; i < 10; ++i){
                field[i][8] = 0;
                recordPanel.score_add(10);
                c8 = false;
            }

        }
        if(c9) {
            for (i = 0; i < 10; ++i) {
                field[i][9] = 0;
                recordPanel.score_add(10);
                c9 = false;
            }

        }
    }

    public void addFigures(){
        //f1 = f2 = f3 = new Figure();
        Random random = new Random();
        f1 = new Figure(random.nextInt(15) + 1);
        if(f1.isCan_turn()){
            f1.turn(random.nextInt(4));
        }
        f2 = new Figure(random.nextInt(15) + 1);
        if(f2.isCan_turn()){
            f2.turn(random.nextInt(4));
        }
        f3 = new Figure(random.nextInt(15) + 1);
        if(f3.isCan_turn()){
            f3.turn(random.nextInt(4));
        }
    }

    public FigurePanel getFigurePanel() {
        return figurepanel;
    }

    public FieldPanel getFieldPanel(){
        return fieldPanel;
    }

    public RecordPanel getRecordPanel() {
        return  recordPanel;
    }
}
