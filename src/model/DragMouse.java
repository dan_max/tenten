/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;


public class DragMouse implements MouseListener {
    private int[] click = new int[4];
    private Model game;
    public DragMouse(Model game_){
        game = game_;
    }

    public void mousePressed(MouseEvent event) {
        click[0] = event.getX();
        click[1] = event.getY();
    }

    public void mouseReleased(MouseEvent event) {
        click[2] = event.getX();
        click[3] = event.getY();
        if(click[2] > 10 && click[2] < 390 && click[3] > 180 && click[3] < 620) {
            game.setFigure(click);
        }

    }

    public void mouseClicked(MouseEvent e) {}    
    public void mouseEntered(MouseEvent event) {} 
    public void mouseExited(MouseEvent event) {} 
}
