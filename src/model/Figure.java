package model;

import java.awt.*;

public class Figure{
    private int[][] table;
    private boolean can_turn;
    private int size_x, size_y;
    private int size;
    public Figure(int type){ ///1 ... 15
        switch (type){
            case 1:
                table = new int [1][1]; //square one
                table[0][0] = 1;
                can_turn = false;
                size = size_x = size_y = 1;
                break;
            case 2:
                table = new int [2][2]; //square two
                table[0][0] = 1;
                table[0][1] = 1;
                table[1][0] = 1;
                table[1][1] = 1;
                can_turn = false;
                size = 4;
                size_x = size_y = 2;
                break;
            case 3:
                table = new int[3][3]; //square three
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                table[1][0] = 1;
                table[1][1] = 1;
                table[1][2] = 1;
                table[2][0] = 1;
                table[2][1] = 1;
                table[2][2] = 1;
                can_turn = false;
                size = 9;
                size_x = size_y = 3;
                break;
            case 4:
                table = new int[2][2]; //'г' two
                table[0][0] = 1;
                table[0][1] = 1;
                table[1][0] = 1;
                table[1][1] = 0;
                can_turn = true;
                size = 3;
                size_x = size_y = 2;
                break;
            case 5:
                table = new int[2][2]; //mirror 'г' two
                table[0][0] = 1;
                table[0][1] = 1;
                table[1][0] = 0;
                table[1][1] = 1;
                can_turn = true;
                size = 3;
                size_x = size_y = 2;
                break;
            case 6:
                table = new int[3][3]; //'г' three
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                table[1][0] = 1;
                table[1][1] = 0;
                table[1][2] = 0;
                table[2][0] = 1;
                table[2][1] = 0;
                table[2][2] = 0;
                can_turn = true;
                size = 5;
                size_x = size_y = 3;
                break;
            case 7:
                table = new int[3][3]; //mirror 'г' three
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                table[1][0] = 0;
                table[1][1] = 0;
                table[1][2] = 1;
                table[2][0] = 0;
                table[2][1] = 0;
                table[2][2] = 1;
                can_turn = true;
                size = 5;
                size_x = size_y = 3;
                break;
            case 8:
                table = new int[1][2]; // hor '-' two
                table[0][0] = 1;
                table[0][1] = 1;
                can_turn = false;
                size = 2;
                size_x = 1;
                size_y = 2;
                break;
            case 9:
                table = new int[2][1]; // ver '-' two
                table[0][0] = 1;
                table[1][0] = 1;
                can_turn = false;
                size = 2;
                size_x = 2;
                size_y = 1;
                break;
            case 10:
                table = new int[1][3]; // hor '-' three
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                can_turn = false;
                size = 3;
                size_x = 1;
                size_y = 3;
                break;
            case 11:
                table = new int[3][1]; // ver '-' three
                table[0][0] = 1;
                table[1][0] = 1;
                table[2][0] = 1;
                can_turn = false;
                size = 3;
                size_x = 3;
                size_y = 1;
                break;
            case 12:
                table = new int[1][4]; // hor '-' four
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                table[0][3] = 1;
                can_turn = false;
                size = 4;
                size_x = 1;
                size_y = 4;
                break;
            case 13:
                table = new int[4][1]; // ver '-' four
                table[0][0] = 1;
                table[1][0] = 1;
                table[2][0] = 1;
                table[3][0] = 1;
                can_turn = false;
                size = 4;
                size_x = 4;
                size_y = 1;
                break;
            case 14:
                table = new int[1][5]; // hor '-' five
                table[0][0] = 1;
                table[0][1] = 1;
                table[0][2] = 1;
                table[0][3] = 1;
                table[0][4] = 1;
                can_turn = false;
                size = 5;
                size_x = 1;
                size_y = 5;
                break;
            case 15:
                table = new int[5][1]; // ver '-' five
                table[0][0] = 1;
                table[1][0] = 1;
                table[2][0] = 1;
                table[3][0] = 1;
                table[4][0] = 1;
                can_turn = false;
                size = 5;
                size_x = 5;
                size_y = 1;
                break;
        }

    }
    
    public Figure(){
        table = new int[5][5];
        for(int i = 0; i < 5 ; ++i){
            for(int j = 0; j < 5 ; ++j){
                table[i][j] = 1;
            }
        }
        can_turn = false;
        size_x = size_y = 5;
    }

    public boolean isCan_turn() {
        return can_turn;
    }

    private void turn_90(){
        for(int i = 0; i < size_x / 2; ++i){
            for(int j = i; j < size_x - 1 - i; ++j){
                int temp = table[i][j];
                table[i][j] = table[size_x - j - 1][i];
                table[size_x - j - 1][i] =  table[size_x - i - 1][size_x - j - 1];
                table[size_x - i - 1][size_x - j - 1] = table[j][size_x - i - 1];
                table[j][size_x - i - 1] = temp;
            }
        }
    }
    public void turn(int type){ //2 ... 3
        for(int i = 0; i < type; i++){
            turn_90();
        }
    }
    public void delete(){
        for(int i = 0; i < size_x; ++i){
            for(int j = 0; j < size_y; ++j){
                table[i][j] = 0;
            }
        }
    }

    public int getSize_x() {
        return size_x;
    }

    public int getSize_y() {
        return size_y;
    }

    public int[][] getTable() {
        return table;
    }

    public int getSize() {
        return size;
    }
}
